# Foundry Server

```sh
make # Build with docker-compose
```

This command builds Traefik and Foundry on docker with docker-compose.
- It also deletes the images to free space.

## How to use (with Docker)
- Go to traefik folder and edit the .env file
  - EMAIL: Just an email for letsencrypt
- Go to foundry folder and edit the .env file
  - DOMAIN and SUBDOMAIN: Of your server
  - FOUNDRY_VERSION: Must match one of the names of .dockerfiles on this folder
    - Example: 0.8.x_alpine
- Download the app from [Foundry VTT](https://foundryvtt.com) inside the **foundry** folder on a folder named **foundryvtt**
- Use the command ```make```

## Backup you data (with Git)

The easiest method is though [GitLab](https://gitlab.com) that allow you to store up to 10 GB.

- Create an account.
- Create a repository.
- Setup a SSH key pair -> [HowTo](https://docs.gitlab.com/ee/ssh/)
- Go to your server foundry folder.
- Setup the **foundrydata** folder as target for this repository.

For automated backups you can use crontab and a small script that make the git instructions (add, commit and push).
- You can find this script in the **scripts/sh** folder called **backup.sh**
- The crontab line should looks like this
```sh
# 4 a.m daily backup
* 4 * * * <path_to_server_deployment_folder>/foundry/foundrydata/backup.sh

# 4 a.m backup on mondays
* 4 * * 0 <path_to_server_deployment_folder>/foundry/foundrydata/backup.sh
```
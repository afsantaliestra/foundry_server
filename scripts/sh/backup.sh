#! /bin/bash

cd "$(dirname "$0")"

git add .
git commit -m "Backup $(date +"%d-%m-%Y %H:%M")"
git push

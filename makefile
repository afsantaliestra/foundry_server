.DEFAULT_GOAL := traefik_foundry_with_clear

traefik_foundry_with_clear: traefik_foundry clear_images

traefik_foundry: create_traefik create_foundry rmb

clear_images: stop rmi start

clear: rmc rmi rmn

create_traefik:
	@echo "\e[44mCreating Traefik Container\e[0m"
	cd traefik; docker-compose up -d

create_foundry:
	@echo "\e[44mCreating Foundry Container\e[0m"
	cd foundry; docker-compose up -d

stop:
	docker stop traefik foundry

start:
	docker start traefik foundry

rmc:
	@echo "\e[44mRemove traefik and foundry containers\e[0m"
	docker rm -f $$(docker ps -a --format "{{.ID}} {{.Names}}" | awk '{if ($$2 ~ /traefik|foundry/) print $$1}')

rmi:
	@echo "\e[44mRemove traefik and foundry images\e[0m"
	docker rmi -f $$(docker images -a --format "{{.ID}} {{.Repository}}" | awk '{if ($$2 ~ /traefik|foundry|node/) print $$1}')

rmn:
	@echo "\e[44mRemove traefik_foundry network\e[0m"
	docker network rm $$(docker network ls --format "{{.ID}} {{.Name}}" | awk '{if ($$2 ~ /traefik_foundry_net/) print $$1}')

rmb:
	@echo "\e[44mRemove builder cache\e[0m"
	docker builder prune -af

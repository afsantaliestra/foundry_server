FROM node:16-alpine

RUN apk add --no-cache tzdata
ENV TZ Europe/Madrid

WORKDIR /usr/src/app/foundryvtt

EXPOSE 30000
CMD ["node", "resources/app/main.js", "--dataPath=/usr/src/app/foundrydata"]
